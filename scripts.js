$(document).ready(function()
{
	var message;
	$("#btnSubmit").click(function()
	{
		message = "";
		var validName = validateName($("#inputName").val());
		var validEmail = validateEmail($("#inputEmail").val());
		var validComments = validateComments($("#areaComments").val());
		console.log(validName);
		console.log(validEmail);
		console.log(validComments);
		if( (validName) && (validEmail) && (validComments) )
		{
			$("#infoText").html("Information was send!.");
			$("#infoText").css("color","Green");
			$("#inputName").val("");
			$("#inputEmail").val("");
			$("#areaComments").val("");
		}
		else
		{
			$("#infoText").html(message);
			$("#infoText").css("color","Red");
		}
		
	});
	
	
	
	function validateName(nameUser)
	{
		if(/^[a-zA-Z\s]+$/.test(nameUser))
		{
			return true;
		}
		else
		{
			message = "The name you entered is invalid. Make sure only content alphabetics.";
			return false;
		}
	}
	
	function validateEmail(emailUser)
	{
		if(/[0-9a-zA-Z\@\.\_]/.test(emailUser))
		{
			if( (emailUser.search("@") > -1) && (emailUser.indexOf(".") > emailUser.indexOf("@")))
			{
				return true;
			}
			else
			{
				if(message === "")
				{
					message = "The email you entered is invalid. Make sure it has proper format (example@domain.com).";
				}
				return false;
			}
		}
		else
		{
			if(message === "")
			{
				message = "The email you entered is invalid. Make sure it has proper format (example@domain.com).";
			}
			return false;
		}
	}
	
	function validateComments(commentsUser)
	{
		if(/[0-9a-zA-Z\s]/.test(commentsUser))
		{
			return true;
		}
		else
		{
			if(message === "")
			{
				message = "The comment you entered is invalid. Make sure the field is not empty or content invalid characters.";
			}
			return false;
		}
	}
	
	$("#btnRead").click(function()
	{
		window.location.href = "secondPage.html";
	});
});